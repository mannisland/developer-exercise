<?php

use App\Soap\StockTicker;

// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['soap.client'] = function ($c) {
    $soapUrl = 'http://soap:8000/server.php?wsdl';

    return new \SoapClient($soapUrl, [
        'cache_wsdl' => WSDL_CACHE_NONE
    ]);
};

$container['db.connection'] = function ($c) {
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false
    ];

    $dsn = 'mysql:host=db;dbname=mif;charset=utf8mb4';

    return new PDO($dsn, 'root', 'mannisland', $options);
};

