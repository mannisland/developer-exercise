<?php

namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

class HomeController
{
    private $c;

    public function __construct($c)
    {
        $this->c = $c;
    }

    public function index(Request $request, Response $response)
    {
        $client = $this->c['soap.client'];
        $result = $client->getCompanies([
            'php-exercise@mannisland.co.uk',
            'p455w0rd'
        ]);

        $renderer = $this->c['renderer'];

        return $renderer->render($response, 'index.phtml', [
            'companies' => $result->item
        ]);
    }
}
