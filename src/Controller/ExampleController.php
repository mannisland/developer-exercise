<?php

namespace App\Controller;

class ExampleController
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function index()
    {
        $db = $this->container['db.connection'];
        $client = $this->container['soap.client'];
    }
}
