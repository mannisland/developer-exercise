<?php

use Slim\Http\Request;
use Slim\Http\Response;
use App\Controller\ExampleController;
use App\Controller\HomeController;

$app->get('/example', ExampleController::class . ':index');

$app->get('/', HomeController::class . ':index');
