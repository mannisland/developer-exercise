<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{
    private $service;

    public function test_index()
    {
        $stockTickerService = $this->getMockBuilder(\SoapClient::class)
            ->disableOriginalConstructor()
            ->setMethods(['getCompanies'])
            ->getMock();

        $stockTickerService->method('getCompanies')
            ->willReturn((object) [
                'item' => [
                    [
                        'symbol' => 'GOOG',
                        'name' => 'Google',
                    ],
                    [
                        'symbol' => 'MSFT',
                        'name' => 'Microsoft'
                    ]
                ]]);

        $this->app->getContainer()['soap.client'] = $stockTickerService;

        $response = $this->runApp('GET', '/');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('GOOG', (string) $response->getBody());
        $this->assertContains('Google', (string) $response->getBody());
        $this->assertContains('MSFT', (string) $response->getBody());
        $this->assertContains('Microsoft', (string) $response->getBody());
    }
}
